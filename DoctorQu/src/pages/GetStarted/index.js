import React from 'react'
import { StyleSheet, Text, View } from 'react-native'
import { ILLogin } from '../../assets'
import { ButtonCs } from '../../commponent/atoms'

const GetStarted = () => {
    return (
        <View style={styles.mainStyle}>
            <View style={styles.ILandTitle}>
            <ILLogin />
            <Text style={styles.title}>Kesehatan dalam genggaman.</Text>
            <Text style={styles.desc}>Konsultasikan kesehatan kepada dokter terpercaya secara langsung menggunakan ponsel.</Text>
            </View>
            <View>
            <ButtonCs title='Get Started'/>
            <View style={{height:10}}/>
            <ButtonCs title='Sign In' type='secondary'/>
            </View>
        </View>
    )
}

export default GetStarted

const styles = StyleSheet.create({
    mainStyle :{
        margin:20,
        justifyContent: 'space-between',
        flex:1,
    },
    title: {
        marginTop: 10,
        fontWeight: 'bold',
        fontSize: 25,
        marginTop: 30,
        textAlign: 'center',
        color: '#00B0FF'
    },
    desc: {
        textAlign: 'center',
        paddingTop: 30,
        fontSize: 15
    },
    ILandTitle: {
        alignItems: 'center',
        marginTop:30,
    }
})
