import React from 'react'
import { StyleSheet, Text, View } from 'react-native'
import { ICSplash } from '../../assets'

export default function SplashScreen() {
    return (
        <View style={styles.bgMain}>
            <ICSplash></ICSplash>
            <Text style={styles.textSplash}>DoctorQu</Text>
        </View>
    )
}

const styles = StyleSheet.create({
    textSplash: {
        fontSize: 25,
        fontWeight: '700',
        color: 'black'
      },
      bgMain: {
        backgroundColor:'#ccffff',
        alignItems: 'center',
        justifyContent: 'center',
        flex: 1
      }
})
