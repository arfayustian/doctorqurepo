import React from 'react'
import { StyleSheet, Text, View } from 'react-native'

const ButtonCs = ({type, title}) => {
    return (
        <View style={styles.container(type)}>
            <Text style={styles.titleStyle(type)}>{title}</Text>
        </View>
    )
}

export default ButtonCs

const styles = StyleSheet.create({
    container:(type) => ({
        paddingVertical: 15,
        backgroundColor: type === 'secondary' ? 'white': '#00B0FF',
        borderRadius:10,
    }),
    titleStyle: (type) => ({
        textAlign: 'center',
        fontWeight: 'bold',
        color: type === 'secondary' ? '#737373' : 'white',
        fontSize: 18
    })
})
